function showProgressDots(numberOfDots) {
 
    var progress = document.getElementById('progressDots');
 
    switch(numberOfDots) {
    		case 1:
            progress.innerHTML = '';
            timerHandle = setTimeout('showProgressDots(2)',500);
            break;
        case 2:
            progress.innerHTML = '.';
            timerHandle = setTimeout('showProgressDots(3)',500);
            break;
        case 3:
            progress.innerHTML = '..';
            timerHandle = setTimeout('showProgressDots(4)',500);
            break;
        case 4:
            progress.innerHTML = '...';
            timerHandle = setTimeout('showProgressDots(1)',500);
            break;
    }
}
window.setTimeout('showProgressDots(1)',100);