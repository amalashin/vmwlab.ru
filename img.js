﻿function Large(obj)
{
    var imgbox=document.getElementById("imgbox");
    imgbox.style.visibility='visible';
    var img = document.createElement("img");    
    img.src=obj.src;
    img.className="popup";
    
    if(img.addEventListener) {
        img.addEventListener('mouseout',Out,false);
    } else {
        img.attachEvent('onmouseout',Out);
    }
    
    var header = document.createElement("span");
    header.innerHTML = obj.alt;
    header.className = "small-text strong";
    imgbox.innerHTML = '';
    imgbox.appendChild(header);
    imgbox.appendChild(img);
    //imgbox.style.height=imgbox.height + header.height;
		imgbox.style.left=(getImgboxLeft(obj,imgbox)) +'px';
    imgbox.style.top=(getImgboxTop(obj,imgbox)) + 'px';    
}

function Out()
{
    document.getElementById("imgbox").style.visibility='hidden';
}

function getImgboxLeft(parentImg,popupImg) {
	return parentImg.offsetLeft - parentImg.width / 2;
}

function getImgboxTop(parentImg,popupImg) {
	return parentImg.offsetTop - parentImg.height / 2;
}